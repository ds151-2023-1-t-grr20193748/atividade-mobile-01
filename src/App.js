import "./App.css";
import { useState } from "react";
const candidates = [
  { name: "Mario", votes: 0 },
  { name: "Luigi", votes: 0 },
  { name: "Toad Blue", votes: 0 },
  { name: "Toad Yellow", votes: 0 },
];
export default function MyApp() {
  const [count, setCount] = useState(0);
  const [winner, setWinner] = useState(null);

  function handleClick(number) {
    setCount(count + 1);
    candidates[number].votes++;
  }

  function calculateVotes() {
    const candidato = candidates.sort((a, b) => a.votes < b.votes);
    setWinner(candidato[0]);
  }

  return (
    <div>
      {winner == null && (
        <div>
          <h1>Contagem dos votos</h1>
          {candidates.map((candidato, i) => (
            <MyButton count={count} onClick={handleClick} candidate={i} />
          ))}
          <button onClick={calculateVotes}> Fechar votação</button>
        </div>
      )}
      {winner != null && (
        <div>
          {candidates.map((candidato, i) => (
            <p>
              {candidato.name}: {candidato.votes}
            </p>
          ))}
          <p>Resultado : {winner.name} é o vencedor!</p>
        </div>
      )}
    </div>
  );
}
function MyButton({ count, onClick, candidate }) {
  return (
    <button
      onClick={() => {
        onClick(candidate);
      }}
    >
      {candidates[candidate].name}
      <h1>Votos: {candidates[candidate].votes}</h1>
    </button>
  );
}
